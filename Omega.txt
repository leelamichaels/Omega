!********************************************************************************!
!                        STUDYING PHOTON TRAJECTORIES                            !
!                                CODE "OMEGA"                                    !
!--------------------------------------------------------------------------------!
!      Last update:                    |          April 17, 2023                 !
!--------------------------------------------------------------------------------!
!  Author: Leela Elpida Koutsantoniou  |     email: leelamk@phys.uoa.gr          !
!--------------------------------------------------------------------------------!
!                                                                                !
!                            D E S C R I P T I O N :                             !
!                           -------------------------                            !  
!                                                                                !
!   This program solves the photon equations of motion in the environment        !
!   surrounding a black hole. This black hole can be either Schwarzschild        !
!   (nonrotating) or Kerr (rotating) by choice of the user.                      !
!                                                                                !
!   When executing the program, the user can select from the popup menus, the    !
!   sliders or the input fields the following options:                           !
!   - Disk model: the geometrical setup of the accretion disk surrounding the    !
!   black hole in the system. Includes no disk, band, disk, slab, wedge, torus.  !
!   - spin: the spin parameter of the central black hole.                        !
!   - band height: how thick we wish the accretion disk to be in the distance    !
!   of the ISCO (Innermost Stable Circular Orbit).                               !
!   - λI: the maximum affine parameter the orbit can reach. For λI=0 the orbit   !
!   has zero length, for λI=-5 medium length, for λI=10 standard length and so   !
!   on.                                                                          !
!   - radius rI: the radial distance of the point of emission/reception from     !
!   the origin, the center of the black hole.                                    !
!   - angle θ: the poloidal angle of the point of emission/reception from the    !
!   z-axis.                                                                      !
!   - polar angle a: the poloidal angle of the photon emission/reception.        !
!   For a=0, the photon is launched toward the center of the black hole. For     !
!   a=180, the photon is emitted radially outward.                               !
!   - azimuthal angle b: the azimuthal angle of the photon emission/reception.   !
!   For b=0, the photon is launched upward and for b=180 downward. For b=90,     !
!   the photon is launched in the equatorial plane for θ=π/2, or the             !
!   corresponding inclined plane otherwise.                                      !
!   - screen size: how far away or close to the system we want to look from.     !
!                                                                                !
!   The black hole event horizon is displayed at the center of the system as a   !
!   black sphere. The dark cyan equatorial ring is the photon orbit. The magenta !
!   equatorial ring is the ISCO. The yellow to brown three-dimensional object is !
!   the accretion disk. Closer to the black hole, the disk is hotter (yellow).   !
!   Further away, it is colder (brown).                                          !
!   The photon trajectory is drawn depending on its point of origin. For a       !
!   photon launched by the accretion disk and reaching the target, the orbit is  !
!   green and the point of origin is a cyan sphere. For a photon supposedly      !
!   reaching the target emitted from the event horizon, the orbit is purple,     !
!   as well as the point of origin sphere. For a photon reaching the target      !
!   emitted from outside the system, the orbit is orange and the supposed point  !
!   of origin a red sphere.                                                      !
!                                                                                !
!   Additional information displayed: the particle’s integrals of motion E       !
!   (total energy), L (angular momentum parallel to symmetry axis), Q (Carter’s  !
!   constant). The emission source coordinates (R, Θ, Φ). Verifications for      !
!   the particle and trajectory status: momentum magnitude should be zero for    !
!   a photon. The x, y and z axes are also displayed measuring distance in       !
!   geometrized units M, the black hole mass.                                    !
!                                                                                !
!   The program's interface is illustrated in Omega.png                          !
!********************************************************************************!
!                                                                                !
!                               HOW TO EXECUTE:                                  !
!                              ----------------                                  !
!                                                                                !
!  1. Unpack "Omega.zip" in the desired location. The folder contains the code   !
!  "Omega.nb" and a short instructions file "README.txt". No additional files    !
!  will be created by this program.                                              !
!  2. Open the notebook *.nb file, using Wolfram Mathematica 9.0.1 or newer.     !
!  3. In order to run the program, we select from the menu                       !
!  "Evaluation" > "Evaluate Notebook".                                           !
!  Can run on Windows or Unix.                                                   !
!********************************************************************************!